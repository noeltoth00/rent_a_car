/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.enums;

/**
 *
 * @author User
 */
public enum RoleEnum {
    ROLE_ADMIN("Administrator"),
    ROLE_USER("User");
    
    private String displayName;
    
    RoleEnum(String displayName) {
        this.displayName = displayName;
    }
    
    public String displayName() {
        return displayName;
    }
}
