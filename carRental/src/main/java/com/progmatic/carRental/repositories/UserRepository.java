/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.repositories;

import com.progmatic.carRental.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author User
 */
public interface UserRepository extends JpaRepository<User, Integer>{
    public User findById(int numb);
    public User findByUsername(String username);
}
