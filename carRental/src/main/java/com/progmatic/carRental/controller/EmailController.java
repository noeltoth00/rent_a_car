package com.progmatic.carRental.controller;

//import com.progmatic.carRental.emailSender.EmailService;
import com.progmatic.carRental.services.EmailService;
import com.progmatic.carRental.services.RentService;
import java.io.IOException;
import java.util.Locale;
import javax.mail.MessagingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EmailController {

    private static final Logger LOG = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    private EmailService emailSender;
    
    @Autowired
    RentService rentservice;
    
    @RequestMapping(value = "/simpleemail/{hash}", method = RequestMethod.GET)
    public String sendMailWithInline(
            @PathVariable("hash") String hash)
            throws MessagingException, IOException {
        Locale locale = Locale.getDefault();
        
        this.emailSender.sendMailWithInline(hash,
                //recipientName, recipientEmail, image.getName(),
                //image.getBytes(), image.getContentType(),
                locale);
        return "redirect:/index";
    }
}
