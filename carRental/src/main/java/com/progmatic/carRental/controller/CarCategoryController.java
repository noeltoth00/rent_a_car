/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.entities.CarCategory;
import com.progmatic.carRental.services.CarCategoryService;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author NoDi
 */
@Controller
public class CarCategoryController {

    @Autowired
    CarCategoryService carCategoryService;

    @RequestMapping(value = {"/newCarCategory"}, method = RequestMethod.GET)
    public String showCategory(Model model) {

        model.addAttribute("car_category", new CarCategory());
        return "newCarCategory";
    }

    @RequestMapping(path = "/newCarCategory", method = RequestMethod.POST)
    public String createCarCategory(CarCategory carCategory) {
        carCategoryService.addCarCategory(carCategory);
        return "redirect:/carCategory";
    }

    @RequestMapping("/carCategory/img/{photoId}.jpg")
    @ResponseBody
    public void img(HttpServletResponse response, @PathVariable int photoId) throws IOException {
        InputStream in = new ByteArrayInputStream(carCategoryService.getCarCategoryPhoto(photoId));
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(in, response.getOutputStream());
    }
}
