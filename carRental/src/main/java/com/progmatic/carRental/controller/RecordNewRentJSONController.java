/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.dto.AvailableCarsDto;
import com.progmatic.carRental.dto.CarCatStartEndDateDto;
import com.progmatic.carRental.dto.RentAndCustomer;
import com.progmatic.carRental.entities.Car;
import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.entities.Rent;
import com.progmatic.carRental.services.CarCategoryService;
import com.progmatic.carRental.services.UnrentedService;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author benko
 */
@RestController
public class RecordNewRentJSONController {
    
    @Autowired
    UnrentedService unrentedService;
    
    @Autowired
    CarCategoryService carCategoryService;
    
    @RequestMapping(value = {"/recordNewRent/availableCars"}, method = RequestMethod.GET)
    public List<AvailableCarsDto> showNewRentPage(
            @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date rentStart,
            @RequestParam @DateTimeFormat(pattern="yyyy-MM-dd") Date rentEnd,
            @RequestParam Integer carCategoryId,
            Model model) {
        
        List<Car> cars = new ArrayList<Car>();
        CarCatStartEndDateDto ccsed = new CarCatStartEndDateDto(rentStart, rentEnd, carCategoryId);
        
        if (ccsed.getCarCategoryId() != 0) {
            cars = unrentedService.freeCars(ccsed.getRentStart(), ccsed.getRentEnd(), ccsed.getCarCategoryId());
        } else {
            cars = unrentedService.freeCars(ccsed.getRentStart(), ccsed.getRentEnd());
        }
        
        List<AvailableCarsDto> availableCars = new ArrayList<>();
        for (Car car : cars) {
            availableCars.add(new AvailableCarsDto(car.getId(),car.getLicencePlate(),car.getType()));
        }
        
        return availableCars;
    }
    
}
