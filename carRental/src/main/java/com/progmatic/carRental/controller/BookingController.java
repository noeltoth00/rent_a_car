/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.entities.Booking;
import com.progmatic.carRental.services.BookingService;
import com.progmatic.carRental.services.CarService;
import com.progmatic.carRental.services.EmailService;
import java.util.Locale;
import javax.mail.MessagingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author NoDi
 */
@Controller
public class BookingController {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    BookingService bookingService;

    @Autowired
    EmailService emailService;

    @RequestMapping(value = {"/bookingList"}, method = RequestMethod.GET)
    public String showBooking(Model model) {
        model.addAttribute("pageTitle", "Booking");
        model.addAttribute("bookingList", bookingService.findBooking());
        return "bookingList";
    }

    @RequestMapping(value = {"/bookingList/add"}, method = RequestMethod.POST)
    public String addBooking(Model mode, Booking booking) throws MessagingException {
        booking.setHashCode(bookingService.HashGenerator());
        Locale locale = Locale.getDefault();
        bookingService.save(booking);
        emailService.sendMailWithInline(booking.getHashCode(), locale);
        return "thankyou";
    }
}
