/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.dto.CarCategoryDto;
import com.progmatic.carRental.services.CarCategoryService;
import com.progmatic.carRental.services.StatisticsService;
import java.text.DateFormatSymbols;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Molnár Richárd
 */
@Controller
public class StatisticsController {

    @Autowired
    StatisticsService ss;

    @Autowired
    CarCategoryService carCategoryService;

    @RequestMapping(value = {"/statistics"}, method = GET)
    public String stat(Model model) {
        model.addAttribute("pageTitle", "Statistics");
        return "statistics";
    }

//    public String getMonth(int month) {
//        return new DateFormatSymbols().getMonths()[month - 1];
//    }

    @RequestMapping(value = {"/chart/{id}"}, method = GET)
    public String finishedStats(@PathVariable("id") String chartId, Model model) {
        List<Integer> years = ss.getYears(chartId);
        model.addAttribute("years", years);
        ss.getMonths(chartId, years.get(0));
        
//        model.addAttribute("me", new StatisticsController());
        if (chartId.equals("finished_car")) {
            model.addAttribute("jsName", "statfinishedsubmit");
            //model.addAttribute("header1", "Brought back Car categories");
            model.addAttribute("header5", "Brought back Car category count by month");
        } else if (chartId.equals("booked_car")) {
            model.addAttribute("jsName", "statbookedsubmit");
            //model.addAttribute("header1", "Booked Car categories");
            model.addAttribute("header5", "Actual booked Car category count by month");
        } else if (chartId.equals("rent_car")){
            model.addAttribute("jsName", "statrentedsubmit");
            //model.addAttribute("header1", "On Rent Car Categories");
            model.addAttribute("header5", "On rent Car categories count by month");
        }
        model.addAttribute("pageTitle", "Statistics");
        return "chart";
    }
    
    @ResponseBody
    @RequestMapping(value = {"/getmonths"}, method = GET)
    public List<Integer> getSelectedYearMonths(
            @RequestParam String chartId,
            @RequestParam int year,
            Model model){
        List<Integer> months = ss.getMonths(chartId, year);
        return months;
    }

    @ResponseBody
    @RequestMapping(value = {"/getfinishedstatistics"}, method = GET)
    public List<CarCategoryDto> getFinishedStatistics(
            @RequestParam int year,
            @RequestParam int month,
            Model model) {

        List<CarCategoryDto> finisheddto = ss.getFinishedCarCategoriesByDate(year, month);

        model.addAttribute("CarCategories", finisheddto);
        model.addAttribute("pageTitle", "Statistics");
        return finisheddto;
    }

    @ResponseBody
    @RequestMapping(value = {"/getbookedstatistics"}, method = GET)
    public List<CarCategoryDto> getBookedStatistics(
            @RequestParam int year,
            @RequestParam int month,
            Model model) {

        List<CarCategoryDto> bookedDto = ss.getBookedCarCategoriesByDate(year, month);

        model.addAttribute("CarCategories", bookedDto);
        model.addAttribute("pageTitle", "Statistics");
        return bookedDto;
    }

    @ResponseBody
    @RequestMapping(value = {"/getonrentstatistics"}, method = GET)
    public List<CarCategoryDto> getOnRentStatistics(
            @RequestParam int year,
            @RequestParam int month,
            Model model) {

        List<CarCategoryDto> onRentDto = ss.getOnRentCarCategoriesByDate(year, month);

        model.addAttribute("CarCategories", onRentDto);
        model.addAttribute("pageTitle", "Statistics");
        return onRentDto;
        
    }
}
