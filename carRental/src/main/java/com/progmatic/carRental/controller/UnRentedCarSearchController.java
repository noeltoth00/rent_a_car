/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.entities.Car;
import com.progmatic.carRental.entities.CarCategory;
import com.progmatic.carRental.entities.UnrentedCarForm;
import com.progmatic.carRental.services.CarCategoryService;
import com.progmatic.carRental.services.UnrentedService;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Kalman
 */
@Controller

public class UnRentedCarSearchController {

    @Autowired
    UnrentedService unrentedService;

    @Autowired
    CarCategoryService carCategoryService;

    @RequestMapping(value = "/unrent", method = RequestMethod.GET)
    public String carFinderPage(Model model, @ModelAttribute(name = "form") UnrentedCarForm form) {
        getCarCategories(model);
        model.addAttribute("pageTitle", "Unrented car finder");
        return "unrentedCarFinder";
    }

    @Transactional
    @RequestMapping(value = "/unrent", method = RequestMethod.POST)
    public String carFinder(Model model, @ModelAttribute(name = "form") UnrentedCarForm form, HttpServletRequest hsr) {
        getCarCategories(model);
        hsr.getSession().setAttribute("form", form);
        List<Car> cars;

        if (form.getSelectedCategoryId() != null && form.getSelectedCategoryId() != 0) {
            cars = unrentedService.freeCars(form.getFrom(), form.getTo(), form.getSelectedCategoryId());
        } else {
            cars = unrentedService.freeCars(form.getFrom(), form.getTo());
        }

        model.addAttribute("cars", cars);

        return "unrentedCarFinder";
    }

    public void getCarCategories(Model model) {
        model.addAttribute("categories", carCategoryService.findAll());
    }
    
    
        
    // külön keresés csak dátumra vagy kategóriára - ok
    // figyelje a dátumokat (a kezdet ne legyen nagyobb a végénél)-ok
    // foglalás gomb a lista elemeire - megvan,de nemjó helyre mutat!
    // adja át az adatokat a megfelelő mezőkbe
    // a keresett dátumok jelenjenek meg vhol - ok
}
