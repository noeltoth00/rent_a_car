/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import static org.springframework.web.bind.annotation.RequestMethod.GET;

/**
 *
 * @author NoDi
 */
@Controller
public class LoginController {
    
    @RequestMapping(method = RequestMethod.GET, value="/login")
    public String login(SecurityContextHolderAwareRequestWrapper request, Model model) {
        boolean a = request.isUserInRole("ROLE_ADMIN");
        boolean u = request.isUserInRole("ROLE_USER");
        if(a || u){
            return "loginAgain";
        }
        
        model.addAttribute("pageTitle","Login");
        return "login";
    }
    
    @GetMapping("/logout")
    public String logout() {
        return "login?logout";
    }
    
    @GetMapping("/welcome")
    public String welcome(Model model) {
       model.addAttribute("pageTitle","Welcome"); 
        return "welcome";
    }

    @GetMapping("/noaccess")
    public String accesDenied() {
        return "noaccess";
    }
    
}
