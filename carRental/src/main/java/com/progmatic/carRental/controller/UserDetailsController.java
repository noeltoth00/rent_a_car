/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.controller;

import com.progmatic.carRental.dto.ChangePasswordDto;
import com.progmatic.carRental.entities.User;
import com.progmatic.carRental.services.UserService;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author Zsolt
 */
@Controller
public class UserDetailsController {

    @Autowired
    UserService userServices;

    @Autowired
    PasswordEncoder PASSWORD_ENCODER;

    @Value("${passwordMinLength}")
    private Integer passwordMinLength;

    @RequestMapping(value = {"/userDetails"}, method = RequestMethod.GET)
    public String showUserDetails(Model model) {
        model.addAttribute("users", userServices.findUser());
        model.addAttribute("pageTitle", "User Details");
        return "userDetails";
    }

        @RequestMapping(value = {"/newUser"}, method = RequestMethod.GET)
    public String newUserDetails(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("pageTitle", "Add New User");
        return "newUser";
    }

    @RequestMapping(value = {"/newUser"}, method = RequestMethod.POST)
    public String newUser(@Valid User user, BindingResult result, Model model) {
        if (user.getPassword().length() < passwordMinLength) {
            result.addError(new FieldError("user", "password", String.format("A jelszónak legalább %d karakternek kell lennie", passwordMinLength)));
        }
        if (result.hasErrors()) {
            return "newUser";
        }
        user.setPassword(PASSWORD_ENCODER.encode(user.getPassword()));
        userServices.save(user);
        return "redirect:/userDetails";
    }

    @RequestMapping(value = {"/editUser/{id}"}, method = RequestMethod.GET)
    public String editUserDetails(@PathVariable("id") Integer userId, Model model) {
        User user = userServices.FindUserById(userId);
        model.addAttribute("user", user);
        model.addAttribute("pageTitle", "User Editor");
        return "userEditor";
    }

    @RequestMapping(value = {"/editUser/{id}"}, method = RequestMethod.POST)
    public String editUserDetails(@Valid User user, BindingResult result, @PathVariable("id") Integer userId, Model model) {
        if (result.hasErrors()) {
            return "userEditor";
        }
        user.setId(userId);
        userServices.save(user);
        return "redirect:/userDetails";
    }

    @RequestMapping(value = {"/changePassword/{id}"}, method = RequestMethod.GET)
    public String changePassword(@PathVariable("id") Integer userId, Model model) {
        model.addAttribute("userId", userId);
        model.addAttribute("changePasswordDto", new ChangePasswordDto());
        model.addAttribute("pageTitle", "Change Password");
        return "userChangePassword";
    }

    @RequestMapping(value = {"/changePassword/{id}"}, method = RequestMethod.POST)
    public String changePassword(@Valid ChangePasswordDto changePasswordDto, BindingResult result, @PathVariable("id") Integer userId, Model model) {
        model.addAttribute("userId", userId);
        if (changePasswordDto.getPassword1().length() < passwordMinLength) {
            result.addError(new FieldError("changePasswordDto", "password1", String.format("A jelszónak legalább %d karakternek kell lennie", passwordMinLength)));
        }
        if (!changePasswordDto.getPassword1().equals(changePasswordDto.getPassword2())) {
            result.addError(new FieldError("changePasswordDto", "password2", "A két jelszónak meg kell egyeznie!"));
        }
        if (result.hasErrors()) {
            return "userChangePassword";
        }
        User user = userServices.FindUserById(userId);
        user.setPassword(PASSWORD_ENCODER.encode(changePasswordDto.getPassword1()));
        userServices.save(user);
        return "redirect:/userDetails";
    }

}
