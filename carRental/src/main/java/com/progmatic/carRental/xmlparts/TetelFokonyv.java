package com.progmatic.carRental.xmlparts;

import java.util.Date;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author User
 */

    // A Számlázz.hu minta xml-nek megfelelő fieldek kellenek!
    // <tetelFokonyv> blokk

public class TetelFokonyv {
    
    @XmlElement
    String gazdasagiEsem;                   //könyvelő dönti el, minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    String gazdasagiEsemAfa;                //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    String arbevetelFokonyviSzam;           //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező) 
    @XmlElement
    String afaFokonyviSzam;                 //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)      
    @XmlElement
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    Date elszDatumTol;                              //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @XmlJavaTypeAdapter(JaxbDateSerializer.class)
    Date elszDatumIg;                               //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)

    public TetelFokonyv() {
    }

    public TetelFokonyv(String gazdasagiEsem, String gazdasagiEsemAfa, String arbevetelFokonyviSzam, String afaFokonyviSzam, 
            Date elszDatumTol, Date eszDatumIg) {
        this.gazdasagiEsem = gazdasagiEsem;
        this.gazdasagiEsemAfa = gazdasagiEsemAfa;
        this.arbevetelFokonyviSzam = arbevetelFokonyviSzam;
        this.afaFokonyviSzam = afaFokonyviSzam;
        this.elszDatumTol = elszDatumTol;
        this.elszDatumIg = eszDatumIg;
    }

    // getter nem lehet (mert duplikációt jelez a generálás során)
    // setter nem szükséges, mert be vannak égetve az adatok
    
}
