package com.progmatic.carRental.xmlparts;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author User
 */

    // A Számlázz.hu minta xml-nek megfelelő fieldek kellenek!
    // <vevo> blokk

public class Vevo {
    
    @XmlElement
    private String nev;
    @XmlElement
    private int irsz;
    @XmlElement
    private String telepules;
    @XmlElement
    private String cim;
    // @XmlElement
    // private String email;            //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
                                        //ha meg van adva, akkor erre az email címre kiküldik a számlát, de TESZT FIÓK ESETÉN BIZTONSÁGI OKOKBÓL NEM KÜLD A RENDSZER EMAILT
    // @XmlElement
    // private boolean sendEmail;       //kérheti, hogy érvényes email cím esetén mégse küldje el az Agent a számlaértesítő email-t
                                        //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    // @XmlElement
    // private int adoalany;            //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private String adoszam;             //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private String postazasiNev;        //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
//    @XmlElement
//    private int postazasiIrsz;        //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private String postazasiTelepules;  //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private String postazasiCim;        //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
//    @XmlElement
//    public VevoFokonyv vevoFokonyv;   //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    
    // @XmlElement
    // String alairoNeve                //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    // @XmlElement
    // int azonosito;                   //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    // @XmlElement
    // String telefonszam;              //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    // @XmlElement
    // String megjegyzes;              //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)

    public Vevo() {
        
    }

    public Vevo(String nev, int irsz, String telepules, String cim, String adoszam) {
        this.nev = nev;
        this.irsz = irsz;
        this.telepules = telepules;
        this.cim = cim;
        this.adoszam = adoszam;
    }

    // getter nem lehet (mert duplikációt jelez a generálás során)
    // setter nem szükséges, mert konstruktorban adjuk át az adatokat, illetve be vannak égetve
    
}
