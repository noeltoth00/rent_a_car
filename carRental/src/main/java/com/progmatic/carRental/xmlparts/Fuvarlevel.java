/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.xmlparts;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author User
 */
    
    // Ezt a részt mi nem használjuk!

    // Ez nem kötelező szekció, ha fuvarlevelet szeretne nyomtatni kérjük vegye fel a kapcsolatot az ügyfélszolgálattal az info@szamlazz.hu email címen. 
    // Kommentelve van a Számlázz.hu xml sablonban
    
    // A Számlázz.hu minta xml-nek megfelelő fieldek kellenek!
    // <fuvarlevel> blokk

public class Fuvarlevel {
    
    @XmlElement
    private int uticel;              // 106 (kommentelve van a Számlázz.hu xml sablonban)
    @XmlElement
    private String futarSzolgalat;   // PPP (kommentelve van a Számlázz.hu xml sablonban)

    public Fuvarlevel() {
    }

    public Fuvarlevel(int uticel, String futarSzolgalat) {
        this.uticel = uticel;
        this.futarSzolgalat = futarSzolgalat;
    }

    // getter nem lehet (mert duplikációt jelez a generálás során)
    
    public void setUticel(int uticel) {
        this.uticel = uticel;
    }

    public void setFutarSzolgalat(String futarSzolgalat) {
        this.futarSzolgalat = futarSzolgalat;
    }
    
    
}
