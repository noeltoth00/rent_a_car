package com.progmatic.carRental.xmlparts;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author User
 */

    // A Számlázz.hu minta xml-nek megfelelő fieldek kellenek!
    // <beallitasok> blokk

public class Beallitasok {
    
    @XmlElement
    private String felhasznalo = "car.rental.progmatic@gmail.com";   //Számlázz.hu-s felhasználó
    @XmlElement
    private String jelszo = "Szamlazz.hujelszo";                    //Számlázz.hu-s felhasználó jelszava
    @XmlElement
    private boolean eszamla = false;         //„true”, ha e-számlát kell készíteni (papír számla, mert az elektronikusan a .pdf)
    @XmlElement
    private String kulcstartojelszo;        //e-számla esetén a kulcstartót nyitó jelszó 
                                            //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private boolean szamlaLetoltes = true;  //„true” ha a válaszban meg szeretnénk kapni az elkészült PDF számlát
    @XmlElement
    private int szamlaLetoltesPld = 1;      //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private int valaszVerzio = 2;           //1: egyszerű szöveges válaszüzenetet vagy pdf-et ad vissza. 
                                            //2: xml válasz, ha kérte a pdf-et az base64 kódolással benne van az xml-ben
    @XmlElement
    private String aggregator;              //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    
    public Beallitasok(){
    }
    
    // getter nem lehet (mert duplikációt jelez a generálás során)
    // setter nem kell, mert be vannak égetve az adatok
}
