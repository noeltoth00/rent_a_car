package com.progmatic.carRental.xmlparts;

import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author User
 */

    // A Számlázz.hu minta xml-nek megfelelő fieldek kellenek!
    // <elado> blokk

public class Elado {
    
    @XmlElement
    private String bank = "Teszt Bank";                                 //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private String bankszamlaszam = "11111111-22222222-33333333";       //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private String emailReplyto = "car.rental.progmatic@gmail.com";     //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)         
    @XmlElement
    private String emailTargy = "Számla";                               //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    @XmlElement
    private String emailSzoveg = "Fizesse ki a számlát.";                  //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)
    // @XmlElement
    // private String alairoNeve                                        //minOccurs=”0” (az XML fájlban opcionális, megadása nem kötelező)

    public Elado() {
    }
    
    // getter nem lehet (mert duplikációt jelez a generálás során)
    // setter nem szükséges, mert be vannak égetve az adatok

}