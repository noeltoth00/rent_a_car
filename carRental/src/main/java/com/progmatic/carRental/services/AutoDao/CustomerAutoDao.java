/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services.AutoDao;

import com.progmatic.carRental.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Molnár Richárd
 */
public interface CustomerAutoDao extends JpaRepository<Customer, Integer> {
    
    
    Customer findCustomerById(Integer id);
}
