/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.Booking;
import com.progmatic.carRental.repositories.BookingRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author NoDi
 */
@Service
public class BookingService {

    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    BookingRepository bookingRepository;

    @Transactional
    public void addBooking(Booking booking) {
        entityManager.persist(booking);
    }

    public List<Booking> findBooking() {
        List resultList = entityManager.createQuery("select b from Booking b")
                .getResultList();
        return resultList;
    }
    
//    public void deleteBooked(Integer bookingId){
//        entityManager.remove(bookingId);
//    }
    
    public void save(Booking b) {
        bookingRepository.save(b);
    }
    
    public Booking findBookingById(int bookingId){
        return bookingRepository.findById(bookingId);
    }
    
    public String HashGenerator() {
        int random = (int) (Math.random() * 99999999 + 10000000);
        return "hash"+random;
    }
}
