/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.entities.Rent;
import com.progmatic.carRental.repositories.RentRepository;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author benko
 */
@Service
public class RecordNewRentServices {
    
    @PersistenceContext
    EntityManager entityManager;

    @Autowired
    RentRepository rentRepository;
    
    @Transactional
    public void addRent(Rent rent) {
        entityManager.persist(rent);
    }
    
    public List<Rent> findRent() {
        List resultList = entityManager.createQuery("select m from rent m")
                .getResultList();
        return resultList;
    }
    
    public Rent findRentById(int id) {
        Rent rentById = rentRepository.findById(id);
        return rentById;
    }

    public void save(Rent r) {
        rentRepository.save(r);
    }

    
}
