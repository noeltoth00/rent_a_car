/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.Booking;
import com.progmatic.carRental.services.AutoDao.CustomerAutoDao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kalman
 */
@Component
public class StoreNoService {
    
    @PersistenceContext
    EntityManager entityManager;
    
    @Autowired
    CustomerAutoDao customerAutoDao;
    
    public List<Booking> bookingHash(String code) {
        return entityManager.createQuery("SELECT b FROM booking b WHERE b.hash_code = :hc")
                .setParameter("hc", code)
                .getResultList();
    }
    
    public Booking FindABookByHash(String hash) {
        Query query = entityManager.createQuery("SELECT b FROM Booking b WHERE b.hashCode = :hashCode")
                .setParameter("hashCode", hash);
        List<Booking> resultList = query.getResultList();
//        if(resultList.isEmpty()){
//        return 
//        }
        return resultList.get(0);
    }
    
    public List<Booking> rentHash(String code) {
        return entityManager.createQuery("SELECT r FROM rent.rent r WHERE r.hash = :hc")
                .setParameter("hc", code)
                .getResultList();
    }
    
    @Transactional
    public void deleted(Booking booking) {
        entityManager.remove(booking);
    }
}
