/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.Car;
import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.entities.Rent;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Controller;

/**
 *
 * @author NoDi
 */
@Controller
public class CounterService {

    @PersistenceContext
    EntityManager entityManager;

    public int countCustomer(List<Customer> list) {
        int cnt = 0;
        for (int i = 0; i < list.size(); i++) {
            cnt++;
        }
        return cnt;
    }

    public int countCars(List<Car> list) {
        int cnt = 0;
        for (int i = 0; i < list.size(); i++) {
            cnt++;
        }
        return cnt;
    }

    public int countRents(List<Rent> list) {
        int cnt = 0;
        for (int i = 0; i < list.size(); i++) {
            cnt++;
        }
        return cnt;
    }
}
