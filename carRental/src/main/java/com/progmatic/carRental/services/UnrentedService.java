/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.Car;
import com.progmatic.carRental.entities.CarCategory;
import com.progmatic.carRental.services.AutoDao.CustomerAutoDao;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author Kalman
 */
@Component
public class UnrentedService {

    @PersistenceContext           
    EntityManager entityManager;

    @Autowired
    CustomerAutoDao customerAutoDao; 

    public List<Car> allCar() { // le kell szűrni másik metódusban+post

        List resultList = entityManager.createQuery("select c from Car c")
                .getResultList();
        return resultList;
    }

    public List<Car> freeCars(Date from, Date to, Integer cc) {
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String fromString = df.format(from);
//        String toString = df.format(to);
        return entityManager.createQuery(
                "SELECT c FROM Car c WHERE c.carCategoryId.id = :carcategory AND c.id NOT IN"
                + "	(SELECT r.carId FROM Rent r WHERE"
                + "     (r.status = 'RENT' OR"
                + "      r.status = 'BOOKED') AND"
                + "    	(r.rentStart BETWEEN :tol AND :ig OR"
                + "    	 r.rentEnd BETWEEN :tol AND :ig OR"
                + "     :tol BETWEEN r.rentStart AND r.rentEnd OR"
                + "    	:ig BETWEEN r.rentStart AND r.rentEnd)"
                + "	)"
                + "GROUP BY c.id")
                .setParameter("tol", from)
                .setParameter("ig", to)
                .setParameter("carcategory", cc)
                .getResultList();
    }

    public List<Car> freeCars(Date from, Date to) {
//        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        String fromString = df.format(from);
//        String toString = df.format(to);
        return entityManager.createQuery(
                "SELECT c FROM Car c WHERE c.id NOT IN"
                + "	(SELECT r.carId FROM Rent r WHERE"
                + "     (r.status = 'RENT' OR"
                + "      r.status = 'BOOKED') AND"
                + "    	(r.rentStart BETWEEN :tol AND :ig OR"
                + "    	 r.rentEnd BETWEEN :tol AND :ig OR"
                + "     :tol BETWEEN r.rentStart AND r.rentEnd OR"
                + "    	:ig BETWEEN r.rentStart AND r.rentEnd)"
                + "	)"
                + "GROUP BY c.id")
                .setParameter("tol", from)
                .setParameter("ig", to)
                .getResultList();
    }
}
