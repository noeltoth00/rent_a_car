/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.services;

import com.progmatic.carRental.entities.Booking;
import com.progmatic.carRental.entities.Email;
import java.util.List;
import java.util.Locale;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

/**
 *
 * @author User
 */
@Component
public class EmailService {

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    TemplateEngine templateEngine;

    @PersistenceContext
    EntityManager entityManager;

    public void sendMailWithInline(final String hashCode,
            /* final String recipientName, final String recipientEmail, final String imageResourceName,
            final byte[] imageBytes, final String imageContentType, */ final Locale locale)
            throws MessagingException {

        // getting the booking data
        Booking booking = FindABookingByHash(hashCode);
        String emailTo = booking.getEmail();
        //String emailTo = "noel.toth@yahoo.com";
        // Prepare the evaluation context
        final Context ctx = new Context(locale);
        //Instantiate an Email object to collect the date

        Email email = new Email(booking.getLastname(), booking.getFirstname(),
                booking.getBookStart(), booking.getBookEnd(), booking.getPlaceStart(),
                booking.getPlaceEnd(), booking.getCarId(), booking.getHashCode());
        ctx.setVariable("email", email);

        // Prepare message using a Spring helper
        final MimeMessage mimeMessage = this.emailSender.createMimeMessage();
        final MimeMessageHelper message
                = new MimeMessageHelper(mimeMessage, true, "UTF-8"); // true = multipart
        message.setSubject("Car Rental booking confirmation");
        message.setFrom("car.rental.progmatic@gmail.com");
        message.setTo(emailTo);

        // Create the HTML body using Thymeleaf
        final String htmlContent = this.templateEngine.process("email-image.html", ctx);
        message.setText(htmlContent, true); // true = isHtml

        // Add the inline image, referenced from the HTML code as "cid:${imageResourceName}"
//        final InputStreamSource imageSource = new ByteArrayResource(imageBytes);
//        message.addInline(imageResourceName, imageSource, imageContentType);
        // Send mail
        this.emailSender.send(mimeMessage);

    }

    public Booking FindABookingByHash(String hashCode) {
        Query queryBooking = entityManager.createQuery("SELECT b FROM Booking b WHERE b.hashCode = :hashCode")
                .setParameter("hashCode", hashCode);
        Query queryRent = entityManager.createQuery("SELECT r FROM Rent r WHERE r.hash = :hashCode")
                .setParameter("hashCode", hashCode);

        List<Booking> resultList = queryBooking.getResultList();
        // Ha a Booking táblában nem találja meg, keresse meg a Rent táblában
        if (resultList.isEmpty()) {
            resultList = queryRent.getResultList();
        }
        if (resultList.isEmpty()) {
            System.out.println("Nincs ilyen hash kód");
        }
        return resultList.get(0);
    }
}
