/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.dto;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;

/**
 *
 * @author benko
 */
public class CarCatStartEndDateDto {
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date rentStart;
    
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date rentEnd;
    
    private Integer carCategoryId;

    public CarCatStartEndDateDto(Date rentStart, Date rentEnd, Integer carCategoryId) {
        this.rentStart = rentStart;
        this.rentEnd = rentEnd;
        this.carCategoryId = carCategoryId;
    }

    public Date getRentStart() {
        return rentStart;
    }

    public void setRentStart(Date rentStart) {
        this.rentStart = rentStart;
    }

    public Date getRentEnd() {
        return rentEnd;
    }

    public void setRentEnd(Date rentEnd) {
        this.rentEnd = rentEnd;
    }

    public Integer getCarCategoryId() {
        return carCategoryId;
    }

    public void setCarCategoryId(Integer carCategoryId) {
        this.carCategoryId = carCategoryId;
    }
    
    
    
}
