/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.dto;

import com.progmatic.carRental.entities.Booking;
import com.progmatic.carRental.entities.CarCategory;
import com.progmatic.carRental.entities.Customer;
import com.progmatic.carRental.entities.Rent;
import javax.persistence.Basic;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.stereotype.Component;

/**
 *
 * @author Jani
 */
@Component
public class RentAndCustomer {
    @Valid
    private Rent rent;
    @Valid
    private Customer customer;
    @Basic(optional = false)
    @NotNull
    private Integer carCatId;
    
    private Integer booking; 

    public Integer getBooking() {
        return booking;
    }

    public void setBooking(Integer id) {
        this.booking = id;
    }

    public Rent getRent() {
        return rent;
    }

    public void setRent(Rent rent) {
        this.rent = rent;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Integer getCarCatId() {
        return carCatId;
    }

    public void setCarCatId(Integer carCatId) {
        this.carCatId = carCatId;
    }
    
}
