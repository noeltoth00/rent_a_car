/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.dto;

import com.progmatic.carRental.entities.Rent;
import java.text.SimpleDateFormat;

/**
 *
 * @author User
 */
public class InvoiceDto {
    
    private SimpleDateFormat keltDate = new SimpleDateFormat("yyyy/MM/dd");
    private SimpleDateFormat teljDate = keltDate;
    private String customerName;
    private String customerCountry;
    private String customerCity;
    private String customerStreet;
    private String customerZip;
    private Rent[] items;

    public SimpleDateFormat getKeltDate() {
        return keltDate;
    }

    public void setKeltDate(SimpleDateFormat keltDate) {
        this.keltDate = keltDate;
    }

    public SimpleDateFormat getTeljDate() {
        return teljDate;
    }

    public void setTeljDate(SimpleDateFormat teljDate) {
        this.teljDate = teljDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerCountry() {
        return customerCountry;
    }

    public void setCustomerCountry(String customerCountry) {
        this.customerCountry = customerCountry;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getCustomerStreet() {
        return customerStreet;
    }

    public void setCustomerStreet(String customerStreet) {
        this.customerStreet = customerStreet;
    }

    public String getCustomerZip() {
        return customerZip;
    }

    public void setCustomerZip(String customerZip) {
        this.customerZip = customerZip;
    }

    public Rent[] getItems() {
        return items;
    }

    public void setItems(Rent[] items) {
        this.items = items;
    }
    
    
    
}
