/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.dto;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

/**
 *
 * @author T. Zsolt
 */

@Component
public class InvoiceXMLDto {
    
    // CUSTOMER
    private String customerName;
    private String customerZip;
    private String customerCity;
    private String customerAddress;
    private String customerTaxFileNo;
    private Integer customerId;
    // POSTING DETAILS
    private String postingName;
    private String postingAddr;
    private String postingCity;
    private String postingZip;
    // EMAIL HEADER
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date issueDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date paymentDueDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date fulfillmentDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date accountingDate;
    private String paymentMethod;
    private String currency;
    private String invoiceLang;
    private String comment;
    // TETELEK
    private String item;
    private int quantity;
    private int unitPrize;
    private int netPize;
    private int grossPrize;
    private String itemComment;

    public InvoiceXMLDto() {
    }
    
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerZip() {
        return customerZip;
    }

    public void setCustomerZip(String customerZip) {
        this.customerZip = customerZip;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getCustomerAddress() {
        return customerAddress;
    }

    public void setCustomerAddress(String customerAddress) {
        this.customerAddress = customerAddress;
    }

    public String getCustomerTaxFileNo() {
        return customerTaxFileNo;
    }

    public void setCustomerTaxFileNo(String customerTaxFileNo) {
        this.customerTaxFileNo = customerTaxFileNo;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getPostingName() {
        return postingName;
    }

    public void setPostingName(String postingName) {
        this.postingName = postingName;
    }

    public String getPostingAddr() {
        return postingAddr;
    }

    public void setPostingAddr(String postingAddr) {
        this.postingAddr = postingAddr;
    }

    public String getPostingCity() {
        return postingCity;
    }

    public void setPostingCity(String postingCity) {
        this.postingCity = postingCity;
    }

    public String getPostingZip() {
        return postingZip;
    }

    public void setPostingZip(String postingZip) {
        this.postingZip = postingZip;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(Date paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }

    public Date getFulfillmentDate() {
        return fulfillmentDate;
    }

    public void setFulfillmentDate(Date fulfillmentDate) {
        this.fulfillmentDate = fulfillmentDate;
    }

    public Date getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(Date accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getInvoiceLang() {
        return invoiceLang;
    }

    public void setInvoiceLang(String invoiceLang) {
        this.invoiceLang = invoiceLang;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getUnitPrize() {
        return unitPrize;
    }

    public void setUnitPrize(int unitPrize) {
        this.unitPrize = unitPrize;
    }

    public int getNetPize() {
        return netPize;
    }

    public void setNetPize(int netPize) {
        this.netPize = netPize;
    }

    public int getGrossPrize() {
        return grossPrize;
    }

    public void setGrossPrize(int grossPrize) {
        this.grossPrize = grossPrize;
    }

    public String getItemComment() {
        return itemComment;
    }

    public void setItemComment(String itemComment) {
        this.itemComment = itemComment;
    }
    
    
}
