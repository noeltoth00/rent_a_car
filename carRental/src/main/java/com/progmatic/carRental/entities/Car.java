/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.entities;

import com.progmatic.carRental.enums.EngineTypeEnum;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NoDi
 */
@Entity
@Table(name = "car")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Car.findAll", query = "SELECT c FROM Car c")
    , @NamedQuery(name = "Car.findById", query = "SELECT c FROM Car c WHERE c.id = :id")
    , @NamedQuery(name = "Car.findByLicencePlate", query = "SELECT c FROM Car c WHERE c.licencePlate = :licencePlate")
    , @NamedQuery(name = "Car.findByVinNumber", query = "SELECT c FROM Car c WHERE c.vinNumber = :vinNumber")
    , @NamedQuery(name = "Car.findByEngineNumber", query = "SELECT c FROM Car c WHERE c.engineNumber = :engineNumber")
    , @NamedQuery(name = "Car.findByColor", query = "SELECT c FROM Car c WHERE c.color = :color")
    , @NamedQuery(name = "Car.findByType", query = "SELECT c FROM Car c WHERE c.type = :type")
    , @NamedQuery(name = "Car.findByEngineType", query = "SELECT c FROM Car c WHERE c.engineType = :engineType")
    , @NamedQuery(name = "Car.findByIsDeleted", query = "SELECT c FROM Car c WHERE c.isDeleted = :isDeleted")})
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "licence_plate")
    private String licencePlate;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 17)
    @Column(name = "vin_number", unique = true)
    private String vinNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "engine_number")
    private String engineNumber;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "color")
    private String color;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Column(name = "engine_type")
    private String engineType;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_deleted", columnDefinition = "TINYINT(1)")
    private boolean isDeleted;
    @JoinColumn(name = "car_category_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private CarCategory carCategoryId;
    @OneToMany(mappedBy = "carId")
    private Collection<Rent> rentCollection;

    public Car() {
    }


    public Car(String licencePlate, String vinNumber, String engineNumber, String color, String type, EngineTypeEnum engineType, boolean isDeleted) {
        this.licencePlate = licencePlate;
        this.vinNumber = vinNumber;
        this.engineNumber = engineNumber;
        this.color = color;
        this.type = type;
        this.engineType = engineType.name();
        this.isDeleted = isDeleted;
    }

    public Car(Integer id, String licencePlate, String vinNumber, String engineNumber, String color, String type, String engineType, boolean isDeleted, CarCategory carCategoryId) {
        this.id = id;
        this.licencePlate = licencePlate;
        this.vinNumber = vinNumber;
        this.engineNumber = engineNumber;
        this.color = color;
        this.type = type;
        this.engineType = engineType;
        this.isDeleted = isDeleted;
        this.carCategoryId = carCategoryId;
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public String getVinNumber() {
        return vinNumber;
    }

    public void setVinNumber(String vinNumber) {
        this.vinNumber = vinNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(EngineTypeEnum engineType) {
        this.engineType = engineType.name();
    }

    public boolean getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(boolean isDeleted) {
        this.isDeleted = isDeleted;
    }

    public CarCategory getCarCategoryId() {
        return carCategoryId;
    }

    public void setCarCategoryId(CarCategory carCategoryId) {
        this.carCategoryId = carCategoryId;
    }

    @XmlTransient
    public Collection<Rent> getRentCollection() {
        return rentCollection;
    }

    public void setRentCollection(Collection<Rent> rentCollection) {
        this.rentCollection = rentCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Car)) {
            return false;
        }
        Car other = (Car) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.progmatic.carRental.Car[ id=" + id + " ]";
    }
    
}
