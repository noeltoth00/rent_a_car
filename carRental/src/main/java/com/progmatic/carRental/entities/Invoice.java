/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.progmatic.carRental.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author NoDi
 */
@Entity
@Table(name = "invoice")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Invoice.findAll", query = "SELECT i FROM Invoice i")
    , @NamedQuery(name = "Invoice.findById", query = "SELECT i FROM Invoice i WHERE i.id = :id")
    , @NamedQuery(name = "Invoice.findByInvoiceNumber", query = "SELECT i FROM Invoice i WHERE i.invoiceNumber = :invoiceNumber")
    , @NamedQuery(name = "Invoice.findByIssueDate", query = "SELECT i FROM Invoice i WHERE i.issueDate = :issueDate")
    , @NamedQuery(name = "Invoice.findByPaymentDueDate", query = "SELECT i FROM Invoice i WHERE i.paymentDueDate = :paymentDueDate")
    , @NamedQuery(name = "Invoice.findByAmount", query = "SELECT i FROM Invoice i WHERE i.amount = :amount")})
public class Invoice implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = true)
    //@NotNull
    @Column(name = "invoice_number")
    private String invoiceNumber;
    @Basic(optional = false)
    @NotNull
    @Column(name = "issue_date")
    @Temporal(TemporalType.DATE)
    private Date issueDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "payment_due_date")
    @Temporal(TemporalType.DATE)
    private Date paymentDueDate;
    @Basic(optional = false)
    @NotNull
    @Column(name = "amount")
    private int amount;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "invoiceId")
    private Collection<Rent> rentCollection;
    @Lob
    @Column(name = "invoice_pdf")
    private byte[] invoicePdf;
//    @Column(name = "invoice_xml")
//    private String invoiceXml;
    @OneToMany(mappedBy = "extraInvoiceId")
    private Collection<Rent> rentCollection1;

    public Invoice() {
    }

    public Invoice(String invoiceNumber, Date issueDate, Date paymentDueDate, int amount) {
        this.invoiceNumber = invoiceNumber;
        this.issueDate = issueDate;
        this.paymentDueDate = paymentDueDate;
        this.amount = amount;
    }
    
    public Invoice(int id, Date issueDate, Date paymentDueDate, int amount) {
        this.id = id;
        this.issueDate = issueDate;
        this.paymentDueDate = paymentDueDate;
        this.amount = amount;
    }
    
    public Invoice(Date issueDate, Date paymentDueDate, int amount) {
        this.issueDate = issueDate;
        this.paymentDueDate = paymentDueDate;
        this.amount = amount;
    }
    
    public byte[] getInvoicePdf() {
        return invoicePdf;
    }

    public void setInvoicePdf(byte[] invoicePhoto) {
        this.invoicePdf = invoicePhoto;
    }
    
//    public String getInvoiceXml() {
//        return invoiceXml;
//    }
//
//    public void setInvoiceXml (String invoiceXml) {
//        this.invoiceXml = invoiceXml;
//    }

    public Integer getId() {
        return id;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public Date getPaymentDueDate() {
        return paymentDueDate;
    }

    public void setPaymentDueDate(Date paymentDueDate) {
        this.paymentDueDate = paymentDueDate;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    @XmlTransient
    public Collection<Rent> getRentCollection() {
        return rentCollection;
    }

    public void setRentCollection(Collection<Rent> rentCollection) {
        this.rentCollection = rentCollection;
    }

    @XmlTransient
    public Collection<Rent> getRentCollection1() {
        return rentCollection1;
    }

    public void setRentCollection1(Collection<Rent> rentCollection1) {
        this.rentCollection1 = rentCollection1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Invoice)) {
            return false;
        }
        Invoice other = (Invoice) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.progmatic.carRental.Invoice[ id=" + id + " ]";
    }
    
}
