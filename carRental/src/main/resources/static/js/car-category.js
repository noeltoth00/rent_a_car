$(function () {
    $('#photoModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var title = button.data('title') // Extract info from data-* attributes
        var uri = button.data('uri') // Extract info from data-* attributes
        var modal = $(this)
        modal.find('.modal-title').text(title)
        modal.find('.modal-body img').attr("src", uri)
    })
});