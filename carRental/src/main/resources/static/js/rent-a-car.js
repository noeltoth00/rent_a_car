$(function () {
    $('.datepicker').datetimepicker({
        format: 'yyyy-mm-dd',
        autoclose: true,
        minView: 2
    });

    $('#start-date-input')
            .datetimepicker()
            .on('changeDate', function (ev) {
                $('#end-date-input').datetimepicker('setStartDate', ev.date);
            });

    $('#end-date-input')
            .datetimepicker()
            .on('changeDate', function (ev) {
                $('#start-date-input').datetimepicker('setEndDate', ev.date);
            });
});