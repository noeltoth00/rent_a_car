// function fetchAvailableCars() {
//   var rentStart = $('#newrent-rentStart').val();
//   var rentEnd = $('#newrent-rentEnd').val();
//   var carCategoryId = $('#newrent-carCatId').val();
//
//   if (rentStart && rentEnd && carCategoryId) {
//     $.ajax({
//       url : '/recordNewRent/availableCars',
//       data : {
//         rentStart : rentStart,
//         rentEnd : rentEnd,
//         carCategoryId : carCategoryId
//       },
//       method : 'GET',
//       success : function( data ){
//         var $carIdSelect = $('#newrent-carId');
//         $carIdSelect.prop('disabled', false);
//         $carIdSelect.children().not(':first-child').remove();
//         data.forEach(function(car){
//           var $newOption = $('<option></option>');
//           $newOption.text( car.type + ' (' + car.licencePlate + ')' );
//           $newOption.attr('value', car.id);
//           $carIdSelect.append( $newOption );
//         });
//       },
//     });
//   }
// }

function fetchAvailableCarsAndSelectCar() {
  var rentStart = $('#newrent-rentStart').val();
  var rentEnd = $('#newrent-rentEnd').val();
  var carCategoryId = $('#newrent-carCatId').val();

  if (rentStart && rentEnd && carCategoryId) {
    $.ajax({
      url : '/recordNewRent/availableCars',
      data : {
        rentStart : rentStart,
        rentEnd : rentEnd,
        carCategoryId : carCategoryId
      },
      method : 'GET',
      success : function( data ){
        var $carIdSelect = $('#newrent-carId');
        $carIdSelect.prop('disabled', false);
        $carIdSelect.children().not(':first-child').remove();
        if (data.length == 0) {
            $carIdSelect.children().first().text("Nincs a keresésnek megfelelő autó");
        } else {
            $carIdSelect.children().first().text("Válassz autót");
            var carId = $("#hiddenCarId").val();
            data.forEach(function(car){
              var $newOption = $('<option></option>');
              $newOption.text( car.type + ' (' + car.licencePlate + ')' );
              $newOption.attr('value', car.id);
              if (carId == car.id) {
                $newOption.attr('selected', true);
              }
              $carIdSelect.append( $newOption );
            });
        }
      }
    });
  }
}

$(document).ready(function ($) {
  if ($("#newrent-carCatId").val() != 0) {
    fetchAvailableCarsAndSelectCar();
  }

  $('#newrent-rentStart').change( fetchAvailableCarsAndSelectCar );
  $('#newrent-rentEnd').change( fetchAvailableCarsAndSelectCar );
  $('#newrent-carCatId').change( fetchAvailableCarsAndSelectCar );

  $('.date').datetimepicker({
    format: 'yyyy-mm-dd hh:ii',
    autoclose: true
  });
});
